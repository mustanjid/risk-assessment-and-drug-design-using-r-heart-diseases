#Using a package named r entrez for collectiing ncbi gene ids
install.packages("rentrez")
library(rentrez)
entrez_dbs()
entrez_db_searchable("gene") #need gene db

r_search1 <- entrez_search(db = "gene", term = "Ischemic[Heart Disease]", retmax=700)
r_search1   #1931

r_search2 <- entrez_search(db = "gene", term = "Cerebrovascular[Heart Disease]", retmax=700)
r_search2  #227


r_search3 <- entrez_search(db = "gene", term = "hypertensive[Heart Disease]", retmax=700)
r_search3  #681

r_search4 <- entrez_search(db = "gene", term = "inflammatory[Heart Disease]", retmax=700)
r_search4  #13743

r_search5 <- entrez_search(db = "gene", term = "Rheumatic[Heart Disease]", retmax=700)
r_search5  #220
